// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

Vue.config.productionTip = false
/* eslint-disable no-new */
if(typeof Notification != 'undefined')Notification.requestPermission();
var notification;
const as = new AthenaSocket(function (object) {
    if(object.type == 998){
        if(this.vm.$route.path == '/chat/waiting'){
            this.vm.$router.go(0);
        }else{
            this.vm.$router.push({path:'/chat/waiting'})
        }
    }else{
        notify(object)
    }
});

function notify(object) {
    if (typeof Notification != 'undefined' && Notification.permission == "granted") {
        var content = '您有一个新消息';
        var request = {icon: '/imgs/messages.png'};
        if (notification) {
            notification.close();
        }
        notification = new Notification(content, request);
    }
}
as.onLoad(function () {
    Vue.prototype.as = this;
    this.customTypes = [];
    const that = this;

    window.addEventListener("message", function( e ) {
        const type = e.data.type;
        const content = e.data.content;
        let ret = false;
        for(const i in that.customTypes){
            try{
                const customType = that.customTypes[i];
                if(customType.type == type){
                    const customContent = JSON.parse(customType.struct);
                    ret = true;
                    for(const j in customContent){
                        const ntype = customContent[j];
                        if(ntype == 'number'){
                            if(!(typeof content[j] == 'number')){
                                return;
                            }
                        }else if(!content[j]){
                            return;
                        }
                    }
                }
            }catch (e){
                console.log(e);
            }
        }
        if(ret){
            const listener = as.getPageCustomMessageListener();
            listener(type,JSON.stringify(content));
        }
    }, false );

    $.post('/message/type/find', {}, function (result) {
        if (result instanceof Array) {
            let customTypes = [];
            for (const type in result) {
                let componentName = 'A'+result[type].code;
                Vue.component(componentName,{
                    props: {
                        content: String
                    },
                    created: function() {
                        let struct = result[type].struct;
                        struct = JSON.parse(struct);
                        const content = JSON.parse(this.content);
                        for(const key in struct){
                          this[key] = content[key];
                        }
                    },
                    template: '<div>'+result[type].view+'</div>'
                });
                customTypes.push({
                    type:result[type].code,
                    struct:result[type].struct
                })
            }
            that.customTypes = customTypes;
        }
    });
  this.vm = new Vue({
    el: '#app',
    router,
    template: '<App/>',
    components: { App }
  })

});
