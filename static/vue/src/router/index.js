import Vue from 'vue'
import Router from 'vue-router'
import Chat from '@/components/Chat'
import Current from '@/components/chat/Current'
import Waiting from '@/components/chat/Waiting'
import Cancel from '@/components/chat/Cancel'
import Settings from '@/components/setting/Setting'
import Base from '@/components/setting/base/Base'
import Advance from '@/components/setting/base/Advance'
import MsgType from '@/components/setting/base/MsgType'
import Structure from '@/components/setting/Structure/Main'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/', redirect: '/chat/current'
    },
    {
      path: '/chat',
      redirect: '/chat/current',
      name: 'Chat',
      component: Chat,
      children: [
        {
          path: 'settings', name: 'settings', component: Settings,redirect: '/chat/settings/basic',
          children: [
              {
                  path: 'basic', name: 'basic', component: Base
              },
              {
                  path: 'advance', name: 'advance', component: Advance
              },
              {
                  path: 'msgtype', name: 'msgtype', component: MsgType
              },
              {
                  path: 'structure', name: 'structure', component: Structure
              }
          ]
        },
        {path: 'current', name: 'current', component: Current},
        {path: 'waiting', name: 'waiting', component: Waiting},
        {path: 'cancel', name: 'cancel', component: Cancel},
      ]
    },
  ]
})
