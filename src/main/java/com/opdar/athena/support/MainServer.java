package com.opdar.athena.support;

import com.opdar.platform.core.base.Context;
import com.opdar.platform.core.base.DispatcherServlet;
import com.opdar.platform.core.base.GulosityServer;
import com.opdar.platform.core.base.ServletEventListener;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;

/**
 * Created by shiju on 2017/1/23.
 */
public class MainServer {
    public static void main(String[] args) {
        Context.putResourceMapping("/css","classpath:/static/css");
        Context.putResourceMapping("/fonts","classpath:/static/fonts");
        Context.putResourceMapping("/imgs","classpath:/static/imgs");
        Context.putResourceMapping("/js","classpath:/static/js");

        Server server = new Server(8081);
        ServletContextHandler context = new ServletContextHandler(1);
        context.addEventListener(new ServletEventListener());
        context.setContextPath("/");
        context.addServlet(DispatcherServlet.class, "/");
        server.setHandler(context);
        try {
            server.start();
            server.join();
        } catch (Exception var3) {
            var3.printStackTrace();
        }
    }
}
