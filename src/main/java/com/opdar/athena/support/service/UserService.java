package com.opdar.athena.support.service;

import com.alibaba.fastjson.JSONObject;
import com.opdar.athena.support.entities.UserEntity;
import com.opdar.athena.support.mapper.UserMapper;
import com.opdar.athena.support.utils.MessageUtils;
import com.opdar.platform.utils.SHA1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.sql.Timestamp;
import java.util.UUID;

/**
 * Created by shiju on 2017/7/11.
 */
@Service
public class UserService {
    @Autowired
    UserMapper userMapper;
    @Autowired
    private MessageUtils messageUtils;


    public boolean regist(String userName,String userPwd){
        if(StringUtils.isEmpty(userName) || StringUtils.isEmpty(userPwd))return false;
        UserEntity user = new UserEntity();
        user.setUserName(userName);
        if(userMapper.count(user) == 0){
            String salt = UUID.randomUUID().toString();
            userPwd = SHA1.encrypt(userPwd+salt);
            user.setId(UUID.randomUUID().toString());
            user.setUserPwd(userPwd);
            user.setSalt(salt);
            user.setForbid(0);
            user.setMessagePwd(UUID.randomUUID().toString());
            JSONObject object = messageUtils.regist(user.getId(), user.getMessagePwd());
            if(object!= null){
                user.setMessageId(object.getString("id"));
            }
            user.setCreateTime(new Timestamp(System.currentTimeMillis()));
            user.setUpdateTime(new Timestamp(System.currentTimeMillis()));
            return userMapper.insert(user) >0;
        }
        return false;
    }

    public boolean update(UserEntity user){
        if(StringUtils.isEmpty(user.getId()))return false;
        UserEntity where = new UserEntity();
        where.setId(user.getId());
        user.setId(null);
        user.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        return userMapper.update(user,where) > 0;
    }

    public UserEntity login(String userName, String userPwd){
        if(StringUtils.isEmpty(userName) || StringUtils.isEmpty(userPwd))return null;
        UserEntity user = new UserEntity();
        user.setUserName(userName);
        user = userMapper.selectOne(user);
        userPwd = SHA1.encrypt(userPwd+user.getSalt());
        if(user.getUserPwd().equals(userPwd)){
            user.setUserPwd(null);
            user.setSalt(null);
            return user;
        }
        return null;
    }

    public UserEntity anonymous(String userName) {
        if(StringUtils.isEmpty(userName)){
            userName = UUID.randomUUID().toString();
        }
        UserEntity user = new UserEntity();
        user.setUserName(userName);
        user = userMapper.selectOne(user);
        if(user == null){
            UserEntity userEntity = new UserEntity();
            userEntity.setId(UUID.randomUUID().toString());
            userEntity.setCreateTime(new Timestamp(System.currentTimeMillis()));
            userEntity.setUpdateTime(new Timestamp(System.currentTimeMillis()));
            userEntity.setUserName(userName);
            userEntity.setAnonymous(1);
            userEntity.setMessagePwd(UUID.randomUUID().toString());
            JSONObject object = messageUtils.regist(userEntity.getId(), userEntity.getMessagePwd());
            if(object!= null){
                userEntity.setMessageId(object.getString("id"));
            }
            userMapper.insert(userEntity);
            user = userEntity;
        }
        return user;
    }
}
