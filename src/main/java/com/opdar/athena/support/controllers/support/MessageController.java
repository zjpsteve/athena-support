package com.opdar.athena.support.controllers.support;

import com.opdar.athena.support.base.CommonInterceptor;
import com.opdar.athena.support.base.Result;
import com.opdar.athena.support.entities.*;
import com.opdar.athena.support.mapper.MessageTypeMapper;
import com.opdar.athena.support.service.MessageService;
import com.opdar.platform.annotations.Interceptor;
import com.opdar.platform.annotations.Request;
import com.opdar.platform.core.base.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;

import java.sql.Timestamp;
import java.util.*;

/**
 * Created by shiju on 2017/7/13.
 */
@Controller
@Interceptor(CommonInterceptor.class)
public class MessageController {

    @Autowired
    MessageTypeMapper messageTypeMapper;
    @Autowired
    MessageService messageService;

    @Request(value = "/message/type/find", format = Request.Format.JSON)
    public Object messageTypeFind() {
        SupportUserEntity user = (SupportUserEntity) Context.getRequest().getAttribute("user");
        String appId = user.getAppId();
        MessageTypeEntity messageTypeEntity = new MessageTypeEntity();
        messageTypeEntity.setAppId(appId);
        List<MessageTypeEntity> types = messageTypeMapper.selectList(messageTypeEntity);
        return Result.valueOf(types);
    }

    @Request(value = "/message/type/delete", format = Request.Format.JSON)
    public Object messageTypeDelete(String id) {
        if (StringUtils.isEmpty(id)) {
            return Result.valueOf(1, "");
        }
        SupportUserEntity user = (SupportUserEntity) Context.getRequest().getAttribute("user");
        String appId = user.getAppId();
        MessageTypeEntity messageTypeEntity = new MessageTypeEntity();
        messageTypeEntity.setAppId(appId);
        messageTypeEntity.setId(id);
        int ret = messageTypeMapper.delete(messageTypeEntity);
        return ret > 0 ? Result.valueOf(id) : Result.valueOf(1, "");
    }

    @Request(value = "/message/type/update", format = Request.Format.JSON)
    public Object messageTypeUpdate(String id, String name, String struct, String view) {
        MessageTypeEntity messageTypeEntity = new MessageTypeEntity();
        if (!StringUtils.isEmpty(id)) {
            MessageTypeEntity where = new MessageTypeEntity();
            where.setId(id);
            MessageTypeEntity update = new MessageTypeEntity();
            update.setUpdateTime(new Timestamp(System.currentTimeMillis()));
            update.setName(name);
            update.setStruct(struct);
            update.setView(view);
            int ret = messageTypeMapper.update(update, where);
            if (ret > 0) {
                messageTypeEntity.setId(id);
                return Result.valueOf(messageTypeMapper.selectOne(messageTypeEntity));
            }
        }
        id = UUID.randomUUID().toString();
        messageTypeEntity.setId(id);
        SupportUserEntity user = (SupportUserEntity) Context.getRequest().getAttribute("user");
        String appId = user.getAppId();
        messageTypeEntity.setName(name);
        messageTypeEntity.setStruct(struct);
        messageTypeEntity.setView(view);
        messageTypeEntity.setAppId(appId);
        messageTypeEntity.setCreateTime(new Timestamp(System.currentTimeMillis()));
        messageTypeEntity.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        Integer code = messageTypeMapper.selectMaxCodeByAppId(appId);
        if (code == null) code = 1008;
        messageTypeEntity.setCode(code + 1);
        messageTypeMapper.insert(messageTypeEntity);
        return Result.valueOf(messageTypeEntity);
    }


    @Request(value = "/message/send", format = Request.Format.JSON)
    public Result sendMessage(String conversationId, String content, Integer type, String reciver, String fakeId, String socktoken) {
        Object user = Context.getRequest().getAttribute("user");
        String tip = null;
        if (user instanceof UserEntity) {
            tip = "您的会话已结束，如需继续交流，请点击<a href=\"javascript:vm.findSupport()\" style=\"font-weight: bold;color: #e0f8f9;font-size: 16px;padding: 4px;\">这里</a>或刷新页面重新发起会话";
        } else if (user instanceof SupportUserEntity) {
            tip = "您的会话已结束，如需查看沟通记录，请至已结束会话查看。";
        }
        Object message = messageService.send(conversationId, content, type, reciver, fakeId, socktoken, tip);
        if (message != null) {
            return Result.valueOf(message);
        }
        return Result.valueOf(1, "");
    }

    @Request(value = "/message/transfer", format = Request.Format.JSON)
    public Result transferConversation(String conversationId, String reciverId, String remark, String socktoken) {
        SupportUserEntity user = (SupportUserEntity) Context.getRequest().getAttribute("user");
        Object message = messageService.transfer(user.getId(), conversationId, reciverId, remark, socktoken);
        if (message != null) {
            return Result.valueOf(message);
        }
        return Result.valueOf(1, "");
    }
}
