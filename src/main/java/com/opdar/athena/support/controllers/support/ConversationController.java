package com.opdar.athena.support.controllers.support;

import com.opdar.athena.support.base.CommonInterceptor;
import com.opdar.athena.support.base.Result;
import com.opdar.athena.support.entities.ConversationEntity;
import com.opdar.athena.support.entities.SupportUserEntity;
import com.opdar.athena.support.mapper.ConversationMapper;
import com.opdar.athena.support.mapper.UserMapper;
import com.opdar.platform.annotations.Interceptor;
import com.opdar.platform.annotations.Request;
import com.opdar.platform.core.base.Context;
import com.opdar.platform.core.session.ISessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.sql.Timestamp;

/**
 * Created by shiju on 2017/7/14.
 */
@Controller
@Interceptor(CommonInterceptor.class)
public class ConversationController {
    @Autowired
    ISessionManager<SupportUserEntity> sessionManager;

    @Autowired
    ConversationMapper conversationMapper;

    /**
     *
     * 客服的会话列表
     * @param stat 客服会话状态
     * @return
     */
    @Request(value = "/conversation/list", format = Request.Format.JSON)
    public Result findConversationList(Integer stat, Boolean isUser){
        SupportUserEntity user = (SupportUserEntity) Context.getRequest().getAttribute("user");
        ConversationEntity where = new ConversationEntity();
        where.setSupportId(user.getId());
        where.setStat(stat);
        if(isUser==null || !isUser)
            return Result.valueOf(conversationMapper.selectList(where));
        else return Result.valueOf(conversationMapper.selectByUser(where));
    }

    @Request(value = "/conversation/get", format = Request.Format.JSON)
    public Result getConversation(String uid){
        SupportUserEntity user = (SupportUserEntity) Context.getRequest().getAttribute("user");
        ConversationEntity where = new ConversationEntity();
        where.setSupportId(user.getId());
        where.setUserId(uid);
        ConversationEntity conversation = conversationMapper.selectOne(where);
        return Result.valueOf(conversation);
    }

    /**
     * 开始沟通
     * @return
     */
    @Request(value = "/conversation/start", format = Request.Format.JSON)
    public Result startConversation(String conversationId){
        Timestamp lastTime = new Timestamp(System.currentTimeMillis());
        {
            //查询最后一个会话
            SupportUserEntity user = (SupportUserEntity) Context.getRequest().getAttribute("user");
            ConversationEntity where = new ConversationEntity();
            where.setSupportId(user.getId());
            where.setStat(2);
            ConversationEntity conversation = conversationMapper.selectOne(where);
            if(conversation != null&& conversation.getEndTime()!=null){
                lastTime = new Timestamp(conversation.getEndTime().getTime()+1000);
            }
        }
        {
            ConversationEntity update = new ConversationEntity();
            update.setStat(1);
            update.setStartTime(new Timestamp(System.currentTimeMillis()));
            update.setLastTime(lastTime);
            ConversationEntity where = new ConversationEntity();
            where.setId(conversationId);
            conversationMapper.update(update,where);
        }
        //可以发送欢迎词
        return Result.valueOf(null);
    }

    @Interceptor(CommonInterceptor.class)
    @Request(value = "/conversation/check", format = Request.Format.JSON)
    public Result checkConversation(String conversationId){
        ConversationEntity where = new ConversationEntity();
        where.setId(conversationId);
        where.setStat(2);
        if(conversationMapper.count(where) > 0){
            return Result.valueOf(1,"");
        }
        return Result.valueOf(null);
    }

    @Interceptor(CommonInterceptor.class)
    @Request(value = "/conversation/stop", format = Request.Format.JSON)
    public Result stopConversation(String conversationId){
        ConversationEntity update = new ConversationEntity();
        update.setStat(2);
        update.setEndTime(new Timestamp(System.currentTimeMillis()));
        ConversationEntity where = new ConversationEntity();
        where.setId(conversationId);
        conversationMapper.update(update,where);
        return Result.valueOf(null);
    }
}
