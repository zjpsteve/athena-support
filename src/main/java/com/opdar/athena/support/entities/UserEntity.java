package com.opdar.athena.support.entities;

import com.opdar.athena.support.mapper.UserMapper;
import com.opdar.plugins.mybatis.annotations.Field;
import com.opdar.plugins.mybatis.annotations.Id;
import com.opdar.plugins.mybatis.annotations.Namespace;

import java.sql.Timestamp;

/**
 * Created by shiju on 2017/7/11.
 */
@Namespace(UserMapper.class)
public class UserEntity {
    @Id
    private String id;
    private String userName;
    private String userPwd;
    private String salt;
    private String nickName;
    private String address;
    private String avatar;
    private String email;
    private String messagePwd;
    private String messageId;
    private Integer forbid;
    private Integer anonymous;
    private String remark;
    @Field(resultmap = false,insert = false,update = false,delete = false,select = false)
    private String appId;
    private Timestamp createTime;
    private Timestamp updateTime;
    @Field(resultmap = false,insert = false,update = false,delete = false,select = false)
    private String utmSource;

    public Integer getAnonymous() {
        return anonymous;
    }

    public void setAnonymous(Integer anonymous) {
        this.anonymous = anonymous;
    }

    public Integer getForbid() {
        return forbid;
    }

    public void setForbid(Integer forbid) {
        this.forbid = forbid;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessagePwd() {
        return messagePwd;
    }

    public void setMessagePwd(String messagePwd) {
        this.messagePwd = messagePwd;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public String getUtmSource() {
        return utmSource;
    }

    public void setUtmSource(String utmSource) {
        this.utmSource = utmSource;
    }
}
