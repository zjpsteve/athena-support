package com.opdar.athena.support.entities;

import java.io.Serializable;

/**
 * Created by shiju on 2017/7/14.
 */
public class WeightEntity implements Serializable{
    private int start;
    private int end;
    private SupportUserEntity support;
    private int current = -1;

    public WeightEntity() {
    }

    @Override
    public String toString() {
        return "WeightEntity{" +
                "start=" + start +
                ", end=" + end +
                '}';
    }

    public WeightEntity(int current) {
        this.current = current;
    }

    public WeightEntity(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public void setSupport(SupportUserEntity support) {
        this.support = support;
    }

    public SupportUserEntity getSupport() {
        return support;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if(o instanceof WeightEntity){
            if(support!=null&&((WeightEntity) o).support.getId().equals(support.getId())){
                return true;
            }
        }
        if(o instanceof WeightEntity){
            if(((WeightEntity) o).start <= current && ((WeightEntity) o).end >= current){
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = start;
        result = 31 * result + end;
        return result;
    }
}
