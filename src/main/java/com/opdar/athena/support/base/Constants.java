package com.opdar.athena.support.base;

/**
 * Created by shiju on 2017/7/11.
 */
public class Constants {
    public static final String APPID = "38c78ece58d74af8994b6cbc49e9fedf";
    public static final String APPSECRET = "43655ea0473a7b9d6d76296884ddcc7fd7175bd1";

    public static final String USER_STATE = "USER_STATE";
    public static final String LAST_TOKEN = "LAST_TOKEN";
    public static final String WORKTIME = "worktime";
    public static final String AUTO_REPLY = "autoReply";
    public static final String AUTO_ADMIT = "autoAdmit";
    public static final String AUTO_TRANSFER = "autoTransfer";
    public static final String QUICK_REPLY = "quickReply";
    public static final class AutoAdmitType{
        public static final int EVERYTIME = 1;
        public static final int WORKTIME = 2;
        public static final int ONLINE = 3;
        public static final int LEAVING = 4;
    }

}
