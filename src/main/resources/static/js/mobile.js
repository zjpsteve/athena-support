var vm = null;
var as = new AthenaSocket(function (object) {
    //文本消息
    if (object.sender == vm.conversation.supportUserEntity.messageId) {
        vm.messages.push(object);
    } else {
        //notification(object)
    }
    scrollToBottom();
});
as.onLoad(function () {
    vm = new Vue({
        el: '#chat',
        data: {
            canLoadMore: 1,
            initStep: 0,
            conversation: null,
            messages: []
        },
        mounted: function () {
            this.findSupport();
            var that = this;

            $('#image-sender').QiniuUpload({
                key:'athena-support',
                callback:function (evt,result) {
                    var url = as.getImageUrl()+result.key;
                    var reciver = vm.conversation.supportUserEntity.messageId;
                    var message = as.send(that.conversation.id,url, reciver,2);
                    vm.messages.push(message);
                    $('.message-content').mCustomScrollbar('scrollTo', 'last');
                }
            },'/upload/token?bucket=athena-support');
        },
        methods: {
            showPrototypeImage:function (event) {
                $(event.target).browser();
            },
            findSupport:function (reload) {
                this.conversation = null;
                var that = this;
                if(!reload)reload = 0;
                $.post('/support/find', {reload:reload}, function (data) {
                    that.lastConversation = that.conversation = data;
                });
            },
            loadMore: function () {
                if (this.messages.length > 0) {
                    vm.canLoadMore = 2;
                    var createTime = new Date(this.messages[0].createTime).format('yyyy-MM-dd hh:mm:ss.S');

                    var oldHeight = $('.message-content .list').scroll().height();
                    var messages = as.messages(this.conversation.userEntity.messageId, this.conversation.supportUserEntity.messageId, null, createTime).reverse();
                    this.messages = messages.concat(this.messages);
                    Vue.nextTick(function () {
                        var newHeight = $('.message-content .list').scroll().height();
                        $('.message-content').scrollTop(newHeight - oldHeight)
                        vm.canLoadMore = 3;
                    });
                }
            }
        },
        watch: {
            conversation: function (conversation) {
                if(conversation==null)return;
                this.messages = as.messages(conversation.userEntity.messageId, conversation.supportUserEntity.messageId).reverse();
                setTimeout(function () {
                    $('.message-content').scrollTop($('.message-content .list').scroll().height());
                }, 0);
                var height = parseInt($('#header').css('height'));
                height += parseInt($('#toolbox').css('height'));
                var clientHeight = $(window).height();
                $('.message-content').css('height', (clientHeight - height - 20) + 'px');
            }
        }
    });
});

function onScroll() {
    var scrollTop = $('.message-content').scrollTop();
    if (scrollTop == 0 && vm.canLoadMore == 3) {
        vm.loadMore();
    }
}

function onKeyDown() {
    if ((event.keyCode || event.which) == 13 && vm.conversation != null) {
        onSend();
        event.preventDefault();
        return false;
    }
}

function onSend() {
    var reciver = vm.conversation.supportUserEntity.messageId;
    var message = as.send(vm.conversation.id,$('#content').text(), reciver);
    vm.messages.push(message);
    $('.message-content').stop();
    $('.message-content').animate({'scrollTop': $('.message-content .list').scroll().height()}, 800);
    $('#content').empty();
}
function scrollToBottom() {
    $('.message-content').stop(true);
    $('.message-content').animate({'scrollTop': $('.message-content .list').scroll().height()}, 800);
}